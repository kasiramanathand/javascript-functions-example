var tipCalculator = function(billAmount){
    var tipAmount;
    if(billAmount < 50){
        tipAmount = billAmount * 0.2;
    }

    else if(billAmount >= 50 && billAmount <= 200){
        tipAmount = billAmount * 0.15;
    }
    else{
        tipAmount = billAmount * 0.1;
    }
    return tipAmount;
}

var bills = [124,48,200];
var tips = [tipCalculator(bills[0]), tipCalculator(bills[1]), tipCalculator(bills[2])]
var totalBill = [bills[0] + tips[0], bills[1] + tips[1], bills[2] + tips[2]]
console.log(tips);
console.log(totalBill);